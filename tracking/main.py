import glob
from tracking.deep_sort.detection import Detection as D
from tracking.utilies.utilies import *
from tracking.tools import generate_detections as gdet
from projection.utilies_proj import *
from tracking.deep_sort import nn_matching
from tracking.deep_sort.tracker import Tracker


model_filename = 'tracking/utilies/mars-small128.pb'
path_save_image = "/data/output/final/"
path_read_image = "/data/images/"

net = detection.net
flag_fr, flag_ri, flag_re, flag_le = False, False, False, False
encoder = gdet.create_box_encoder(model_filename, batch_size=1)

max_cosine_distance = 0.9
nn_budget = 20

metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
tracker = Tracker(metric)

pair_fr_ri, pair_ri_re, pair_re_le, pair_le_fr = [], [], [], []
IDs_list = {}
id_color = {}

frame = 1
for img_name in glob.glob(path_read_image + "*.jpeg"):
    # start detection for all cameras
    if img_name.find("front") != -1:
        flag_fr = True
        boxs_fr, confs_fr, clsses_fr, image_fr = detect_cam(img_name)
        # create descriptor vector for each predicted bounding box
        features_fr = encoder(image_fr, boxs_fr)

    if img_name.find("right") != -1:
        flag_ri = True
        boxs_ri, confs_ri, clsses_ri, image_ri = detect_cam(img_name)
        for i in range(len(boxs_ri)):
            boxs_ri[i][0] += 1280
        features_ri = encoder(image_ri, boxs_ri)

    if img_name.find("rear") != -1:
        flag_re = True
        boxs_re, confs_re, clsses_re, image_re = detect_cam(img_name)
        for i in range(len(boxs_re)):
            boxs_re[i][0] += 1280 * 2
        features_re = encoder(image_re, boxs_re)

    if img_name.find("left") != -1:
        flag_le = True
        boxs_le, confs_le, clsses_le, image_le = detect_cam(img_name)
        for i in range(len(boxs_le)):
            boxs_le[i][0] += 1280 * 3
        features_le = encoder(image_le, boxs_le)
    # concatenate the four images and dorost kardane bounding-box dar forme jadide ax
    if flag_fr and flag_ri and flag_re and flag_le == True:
        im = cv2.hconcat([image_fr, image_ri, image_re, image_le])
        bboxes, confs, clss = boxs_fr + boxs_ri + boxs_re + boxs_le, confs_fr + confs_re + confs_ri + confs_le, clsses_fr + clsses_ri + clsses_re + clsses_le
        features = np.concatenate((features_fr, features_ri, features_re, features_le), axis=0)


        detections = [D(bbox, confs, cls, feature) for bbox, confs, cls, feature in zip(bboxes, confs, clss, features)]
        if detections == None:
            continue

        # start tracking
        tracker.predict()
        tracker.update(detections)
        boxes = []
        ids = []


        for track in tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:
                continue

            bbox = track.to_tlbr()
            boxes.append(bbox)
            bbox_out = bbox.copy()
            print(track.track_id)
            ids.append(track.track_id)

            color = color_id(track.track_id, id_color)
            # fusion the IDs
            new_id = change_ids(track.track_id, IDs_list)


            save_image(frame, image_fr, image_ri, image_re, image_le, bbox_out, new_id, color, path_save_image)


        front, right, rear, left = split_boxes(boxes, ids)

        prj_pts_fr_ri, pts_in_ROI_fr_ri, pts_in_ROI_ri_fr = project_points(right, front, ROI_ri_fr, ROI_fr_ri, Xe_right_front)
        prj_pts_ri_re, pts_in_ROI_ri_re, pts_in_ROI_re_ri = project_points(rear, right,  ROI_re_ri, ROI_ri_re,  Xe_rear_right)
        prj_pts_re_le, pts_in_ROI_re_le, pts_in_ROI_le_re = project_points(left, rear, ROI_le_re, ROI_re_le,   Xe_left_rear)
        prj_pts_le_fr, pts_in_ROI_fr_le, pts_in_ROI_le_fr = project_points(front, left, ROI_fr_le, ROI_le_fr,  Xe_front_left)

        pair_fr_ri = allocation_points(prj_pts_fr_ri, pts_in_ROI_ri_fr,IDs_list)
        pair_ri_re = allocation_points(prj_pts_ri_re, pts_in_ROI_re_ri,IDs_list)
        pair_re_le = allocation_points(prj_pts_re_le, pts_in_ROI_le_re,IDs_list)
        pair_le_fr = allocation_points(prj_pts_le_fr, pts_in_ROI_le_fr,IDs_list)

        flag_fr, flag_ri, flag_re, flag_le = False, False, False, False
        frame += 1