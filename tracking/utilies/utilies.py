
import cv2
import numpy as np
import detection.detection as detection
from projection.utilies_proj import pointInRect



net = detection.net

def detect_cam(image_name):

    img = cv2.imread(image_name)
    bboxes, confs, clss = detection.detect(img, net)

    return bboxes, confs, clss, img

def split_boxes(bbox, id):
    bbox_fr, id_fr, ctr_fr = [], [], []
    bbox_ri, id_ri, ctr_ri = [], [], []
    bbox_re, id_re, ctr_re = [], [], []
    bbox_le, id_le, ctr_le = [], [], []
    front, right, rear, left = [], [], [], []

    for i in range(len(bbox)):


        if bbox[i][0] <= 1280:

            bbox_fr.append(bbox[i])
            id_fr.append(id[i])
            ctr_fr.append([int((bbox[i][0] + bbox[i][2]) / 2), int((bbox[i][1] + bbox[i][3]) / 2)])

        if 1280 < bbox[i][0] <= 1280 * 2:
            bbox[i][0] -= 1280
            bbox[i][2] -= 1280
            bbox_ri.append(bbox[i])
            id_ri.append(id[i])
            ctr_ri.append([int((bbox[i][0] + bbox[i][2]) / 2), int((bbox[i][1] + bbox[i][3]) / 2)])

        if 1280 * 2 < bbox[i][0] < 1280 * 3:
            bbox[i][0] -= 1280 * 2
            bbox[i][2] -= 1280 * 2
            bbox_re.append(bbox[i])
            id_re.append(id[i])
            ctr_re.append([int((bbox[i][0] + bbox[i][2]) / 2), int((bbox[i][1] + bbox[i][3]) / 2)])

        if 1280 * 3 < bbox[i][0]:
            bbox[i][0] -= 1280 * 3
            bbox[i][2] -= 1280 * 3
            bbox_le.append(bbox[i])
            id_le.append(id[i])
            ctr_le.append([int((bbox[i][0] + bbox[i][2]) / 2), int((bbox[i][1] + bbox[i][3]) / 2)])

    front.append((bbox_fr, id_fr, ctr_fr))
    right.append((bbox_ri, id_ri, ctr_ri))
    rear.append((bbox_re, id_re, ctr_re))
    left.append((bbox_le, id_le, ctr_le))
    return front, right, rear, left




def save_image(frame, image_fr, image_ri, image_re, image_le, bounding_box , id, color_box, path_save_image):

    bbox = bounding_box
    if len(color_box) != 0:
        for key, value in color_box.items():
            if key == id:
                color = value

    if pointInRect(bbox[:2], [0, 0, 1280, 800]):

        cv2.rectangle(image_fr, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
        cv2.putText(image_fr, "ID:" + str(id), (int(bbox[0]), int(bbox[1])),fontFace=cv2.FONT_HERSHEY_COMPLEX_SMALL, fontScale=0.9, color=(255, 255, 255), thickness=1,
                        lineType=cv2.LINE_AA)
        cv2.putText(image_fr, "Front" , (int(20), int(770)), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=2, color=(255, 255, 255), thickness=2,
                    lineType=cv2.LINE_AA)

    if pointInRect(bbox[:2], [1280, 0, 1280 * 2, 800]):
        bbox[0] -= 1280
        bbox[2] -= 1280
        cv2.rectangle(image_ri, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
        cv2.putText(image_ri, "ID:" + str(id), (int(bbox[0]), int(bbox[1])), fontFace=cv2.FONT_HERSHEY_COMPLEX_SMALL, fontScale=0.9, color=(255, 255, 255), thickness=1,
                        lineType=cv2.LINE_AA)
        cv2.putText(image_ri, "Right", (int(20), int(780)), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=2, color=(255, 255, 255), thickness=2,
                    lineType=cv2.LINE_AA)

    if pointInRect(bbox[:2], [1280 * 2, 0, 1280 * 3, 800]):
        bbox[0] -= 1280 * 2
        bbox[2] -= 1280 * 2
        cv2.rectangle(image_re, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
        cv2.putText(image_re, "ID:" + str(id), (int(bbox[0]), int(bbox[1])), fontFace=cv2.FONT_HERSHEY_COMPLEX_SMALL, fontScale=0.9, color=(255, 255, 255), thickness=1,
                        lineType=cv2.LINE_AA)
        cv2.putText(image_re, "Rear", (int(20), int(780)), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=2, color=(255, 255, 255), thickness=2,
                    lineType=cv2.LINE_AA)

    if pointInRect(bbox[:2], [1280 * 3, 0, 1280 * 4, 800]):
        bbox[0] -= 1280 * 3
        bbox[2] -= 1280 * 3
        cv2.rectangle(image_le, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
        cv2.putText(image_le, "ID:" + str(id), (int(bbox[0]), int(bbox[1])), fontFace=cv2.FONT_HERSHEY_COMPLEX_SMALL, fontScale=0.9, color=(255, 255, 255), thickness=1,
                        lineType=cv2.LINE_AA)
        cv2.putText(image_le, "Left", (int(20), int(780)), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=2, color=(255, 255, 255), thickness=2,
                    lineType=cv2.LINE_AA)


    scale_percent = 60
    width = int(image_le.shape[1] * scale_percent / 100)
    height = int(image_le.shape[0] * scale_percent / 100)

    dsize = (width, height)

    image_fr = cv2.resize(image_fr, dsize)
    image_ri = cv2.resize(image_ri, dsize)
    image_re = cv2.resize(image_re, dsize)
    image_le = cv2.resize(image_le, dsize)

    hori1 = np.concatenate((image_fr, image_ri), axis=1)
    hori2 = np.concatenate((image_le, image_re), axis=1)
    ver = np.concatenate((hori1, hori2), axis=0)

    cv2.imwrite(path_save_image + "{}.jpeg".format(frame), ver)


