import numpy as np
from scipy.optimize import linear_sum_assignment

 # projection matrix for each adjacent camera from right image to left image
Xe_front_left = np.array([-0.1746, 0.0703, 726.1954, -0.0010, 0.0001, -0.1005, 0.8400, -116.7490, 1.0000])
Xe_left_rear = np.array([-0.5954,-1.4830,689.3473,-0.0012,-0.0020,-0.4088,-0.4737,297.5140,1.0000])
Xe_rear_right = np.array([0.8406,-1.9853,682.6517,0.0008,-0.0027,0.0241,-0.0511,20.1067,1.0000])
Xe_right_front = np.array([-0.1405,-0.5840,894.7065,-0.0008,-0.0003,0.0231,0.3588,120.7696,1.0000])


# the defined ROI for each image
ROI_fr_ri = [800, 100, 1180, 600] # x1, y1, x2, y2
ROI_fr_le = [100, 100, 480,  600]

ROI_ri_fr = [100, 100, 480,  600]
ROI_ri_re = [800, 100, 1180, 600]

ROI_re_ri = [100, 100, 480,  600]
ROI_re_le = [800, 100, 1180, 600]

ROI_le_re = [100, 100, 480,  600]
ROI_le_fr = [800, 100, 1180, 600]

def calculate_point(center, Xe):
    """

    :param center: center of bounding box
    :param Xe: projection matrix
    :return: projects the coordinates of the bounding box center into the neighbouring camera
     """
    x1 , y1 = center[0], center[1]
    x2 = round((np.divide((x1 * Xe[0] + y1 * Xe[1] + Xe[2]), (x1 * Xe[3] + y1 * Xe[4] + Xe[8]))))
    y2 = round((np.divide((x1 * Xe[5] + y1 * Xe[6] + Xe[7]), (x1 * Xe[3] + y1 * Xe[4] + Xe[8]))))

    return int(x2), int(y2)

def pointInRect(point,rect):
    """

    :param point: center of bounding box
    :param rect: the points of ROI (top-left, bottom-right)
    :return: True/False variable whether the point is located inside the ROI or not
    """
    x1, y1, x2, y2 = rect

    x, y = point
    if (x1 < x and x < x2):
        if (y1 < y and y < y2):
            return True
    return False

def project_points(points_right, points_left, ROI_right, ROI_left, matrix_right_to_left):

    """

    :param points_right: the points of bounding-box center in the right image
    :param points_left: the points of bounding-box center in the left image
    :param ROI_right: ROI defined in right image
    :param ROI_left:ROI defined in left image
    :param matrix_right_to_left: projection matrix from right camera to left camera
    :return: the projected point from right camera to left camera
    """
    pts_in_ROI_le = []   # the points that are in ROI from right image
    pts_in_ROI_ri = []   # the points that are in ROI from left image
    prj_pts_ri_le = []   # projected points from right image to left image

    # projection from right image to left image
    for cntr_right, id_right, in zip(points_right[0][2], points_right[0][1]):


        if pointInRect(cntr_right , ROI_right) == True:
            x2, y2 = calculate_point(cntr_right, matrix_right_to_left)
            prj_pts_ri_le.append(np.array([x2, y2, id_right]))
            x1, y1 = cntr_right[0], cntr_right[1]
            pts_in_ROI_ri.append(np.array([x1, y1, id_right]))

    for cntr_left, id_left, in zip(points_left[0][2], points_left[0][1]):

        if pointInRect(cntr_left , ROI_left) == True:

            x1, y1 = cntr_left[0], cntr_left[1]
            pts_in_ROI_le.append(np.array([x1, y1, id_left]))


    return prj_pts_ri_le, pts_in_ROI_ri, pts_in_ROI_le


def allocation_points(prj_pts_ri_le, points_left, IDs_list):

    """

    :param prj_pts_ri_le: the projected point from right camera to left camera
    :param points_left: the points of bounding-box center in the left image
    :param IDs_list: the list of IDs of generated tracker function
    :return: with the help of the hungarian algorithm assigns the correct IDs
    """
    # if projected points and detected points is not equal zero
    if prj_pts_ri_le and points_left != []:

        # change list to array
        prj_pts_ri_le = np.array(prj_pts_ri_le)
        # change list to array
        points_left = np.array(points_left)
        def create_cost(points1, points2):
            ignore_list = []
            cost = np.zeros((points1.shape[0], points2.shape[0]))

            for i in range(len(points1)):
                for j in range(len(points2)):
                    dist = np.linalg.norm(points1[i][:2] - points2[j][:2])
                    if dist > 100:
                        ignore_list.append(sorted([points1[i][2:][-1], points2[j][2:][-1]]))
                    cost[i, j] = dist
            return cost, ignore_list

        if prj_pts_ri_le.shape != points_left.shape:
            cost = create_cost(points_left, prj_pts_ri_le)
        if prj_pts_ri_le.shape == points_left.shape:
            cost = create_cost(prj_pts_ri_le, points_left)

        cost, ignore_list = create_cost(prj_pts_ri_le, points_left)

        row_ind, col_ind = linear_sum_assignment(cost)

        lenght = min(cost.shape[1], cost.shape[0])
        for i in range(lenght):
            pair_id = sorted([prj_pts_ri_le[row_ind[i]][2:][-1], points_left[col_ind[i]][2:][-1]])
            if pair_id in ignore_list:
                continue

            ids = {pair_id[0] : [pair_id[1]]}

            for key, value in ids.items():
                if key in IDs_list.keys():
                    values = [item for sublist in IDs_list.values() for item in sublist]
                    if next(iter(ids.values()))[-1] not in values:
                        IDs_list[key].remove(IDs_list[key][0])
                        IDs_list[key].append(next(iter(ids.values()))[-1])
                else:
                    IDs_list.update(ids)



def change_ids(track_ids, new_track_ids):

    for key, value in new_track_ids.items():
        if track_ids in value:
            track_ids = key
    return track_ids


def color_id(track_id, color_box):

    if track_id not in color_box.keys():
        color = [(184, 134, 11), (255, 255, 5), (238, 221, 130), (238, 207, 161),
                 (238, 130, 238), (85, 26, 139), (160, 32, 240), (238, 174, 238),
                 (255, 52, 179), (238, 92, 66), (238, 0, 0), (139, 26, 26),
                 (255, 106, 106), (255, 165, 0), (238, 118, 0), (0, 139, 0),
                 (0, 255, 127), (32, 178, 170), (64, 64, 64), (47, 79, 79),
                 (139, 69, 19), (0, 255, 255), (30, 144, 255), (0, 0, 0),
                 (240, 128, 128),(210,105,30),(105,105,105),(0,250,154),
                 (224,102,255),(205,186,150),(255,99,71),(238,230,133),(202,255,112)]

        indx = np.random.randint(0, len(color), 1)[-1]

        ID = {track_id: color[indx]}
        color_box.update(ID)

    return color_box