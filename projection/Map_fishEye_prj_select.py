# #This function estimates related coordinates in the second image
# # chose state
# # click in the first image, prerss Enter,
# # related point in the other will be shown

import cv2
import numpy as np


# function to display the coordinates of
# of the points clicked on the image
def click_event(event, x1, y1, flags, params):
    # checking for left mouse clicks
    if event == cv2.EVENT_LBUTTONDOWN:
        # displaying the coordinates
        # on the Shell
        print('(x1,y1) : ')
        print(x1, ',', y1)
        print('')
        # displaying the coordinates
        # on the image window
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(fisheye_img_right, str(x1) + ',' +
                    str(y1), (x1, y1), font,
                    1, (255, 0, 0), 2)
        cv2.imshow('image', fisheye_img_right)
        global xg1
        global yg1
        xg1 = x1
        yg1 = y1


PATH = "/home/pi/fisheye/inputs/"

if __name__ == "__main__":

    # states:
    # 1 front-left
    # 2 left-rear
    # 3 rear_right
    # 4 right_front

    state = 4

    if state == 1:
        Rgt = "front"
        Lft = "left"
        Xe = np.array([-0.1746, 0.0703, 726.1954, -0.0010, 0.0001, -0.1005, 0.8400, -116.7490, 1.0000])

    if state == 2:
        Rgt = "left"
        Lft = "rear"
        Xe = np.array([-0.5954, -1.4830, 689.3473, -0.0012, -0.0020, -0.4088, -0.4737, 297.5140, 1.0000])

    if state == 3:
        Rgt = "rear"
        Lft = "right"
        Xe = np.array([0.8406, -1.9853, 682.6517, 0.0008, -0.0027, 0.0241, -0.0511, 20.1067, 1.0000])

    if state == 4:
        Rgt = "right"
        Lft = "front"
        Xe = np.array([-0.1405, -0.5840, 894.7065, -0.0008, -0.0003, 0.0231, 0.3588, 120.7696, 1.0000])

    print('Xe: ')
    print(Xe[:])

    # reading the images
    img_com_name = "1560848742994386125_"
    fisheye_img_right = cv2.imread(PATH + img_com_name + Rgt + "_camera.jpeg")
    fisheye_img_left = cv2.imread(PATH + img_com_name + Lft + "_camera.jpeg")
    # print(fisheye_img_right.shape)

    # displaying the image
    cv2.imshow('image', fisheye_img_right)
    cv2.waitKey(1)
    # setting mouse handler for the image
    # and calling the click_event() function
    cv2.setMouseCallback('image', click_event)
    # wait for a key to be pressed to exit
    cv2.waitKey(0)

    x1 = xg1
    y1 = yg1
    #     print('(x1,y1) : ')
    #     print(x1, ',', y1)

    ## calculate match points
    x2 = round((np.divide((x1 * Xe[0] + y1 * Xe[1] + Xe[2]), (x1 * Xe[3] + y1 * Xe[4] + Xe[8]))))
    y2 = round((np.divide((x1 * Xe[5] + y1 * Xe[6] + Xe[7]), (x1 * Xe[3] + y1 * Xe[4] + Xe[8]))))
    print('(x2,y2) : ')
    print(x2, ',', y2)
    x2 = int(x2)
    y2 = int(y2)

    h, w = fisheye_img_right.shape[:2]

    if x2 > 0 and y2 > 0 and x2 < w and y2 < h:
        print('related point is within second image')
        # displaying the coordinates
        # on the image window
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(fisheye_img_left, str(x2) + ',' + str(y2), (x2, y2), font, 1, (255, 0, 0), 2)
        cv2.imshow('image', fisheye_img_left)
        cv2.waitKey(0)

    # close the window
    cv2.destroyAllWindows()

    print('The End')
    exit()

