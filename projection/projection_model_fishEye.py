# #This function produces projection model
# # click related point carfully and save points
# # save model and use in test function

import cv2
import numpy as np

PATH = "/home/pi/fisheye/inputs/"


# function to display the coordinates of
# of the points clicked on the image
def click_event(event, x, y, flags, params):
    # checking for left mouse clicks
    if event == cv2.EVENT_LBUTTONDOWN:
        # displaying the coordinates
        # on the Shell
        print('x,y : ')
        print(x, ',', y)
        print('')
        # displaying the coordinates
        # on the image window
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(fisheye_img_right, str(x) + ',' +
                    str(y), (x, y), font,
                    1, (255, 0, 0), 2)
        cv2.imshow('image', fisheye_img_right)
        global xg
        global yg
        xg = x
        yg = y


if __name__ == "__main__":
    # states:
    # 1 front-left
    # 2 left-rear
    # 3 rear_right
    # 4 right_front

    state = 2

    EPN = 4  # Enter_Point_Number
    img_com_name = "1560848742994386125_"

    if state == 1:
        Rgt = "front"
        Lft = "left"

    if state == 2:
        Rgt = "left"
        Lft = "rear"

    if state == 3:
        Rgt = "rear"
        Lft = "right"

    if state == 4:
        Rgt = "right"
        Lft = "front"

    fisheye_img_right = cv2.imread(PATH + img_com_name + Rgt + "_camera.jpeg")
    fisheye_img_left = cv2.imread(PATH + img_com_name + Lft + "_camera.jpeg")

    x1 = np.zeros((EPN, 1), float)
    y1 = np.zeros((EPN, 1), float)
    x2 = np.zeros((EPN, 1), float)
    y2 = np.zeros((EPN, 1), float)

    for pnt in range(0, EPN):
        fisheye_img_left_temp = fisheye_img_left
        fisheye_img_right_temp = fisheye_img_right
        # print(pnt)

        # displaying the image
        cv2.imshow('image', fisheye_img_right_temp)
        # setting mouse handler for the image
        # and calling the click_event() function
        cv2.setMouseCallback('image', click_event)
        # wait for a key to be pressed to exit
        cv2.waitKey(0)

        x1[pnt] = xg
        y1[pnt] = yg
        cv2.destroyAllWindows()

        # displaying the seconde image
        cv2.imshow('image', fisheye_img_left_temp)
        # setting mouse handler for the image
        # and calling the click_event() function
        cv2.setMouseCallback('image', click_event)
        # wait for a key to be pressed to exit
        cv2.waitKey(0)
        x2[pnt] = xg
        y2[pnt] = yg
        print('x2,y2 : ')
        print(x2, ',', y2)
        cv2.destroyAllWindows()

    print('x1 : ')
    print(x1)
    print('y1 : ')
    print(y1)
    print('x2 : ')
    print(x2)
    print('y2 : ')
    print(y2)
    ##for saving goals
    x1y1 = np.hstack((x1, y1))
    x2y2 = np.hstack((x2, y2))
    All_points = np.hstack((x1y1, x2y2))
    print('All_points : ')
    print(All_points)

    Nums = x1.shape[:2]  # has 'EPN' value
    print('input points')
    print(Nums[0])
    Num = Nums[0]
    A = np.zeros((2 * Num, 8), float)
    B = np.zeros((2 * Num, 1), float)


    for i in range(0, Num):
        print(i)
        print(x1[i], y1[i], x2[i], y2[i])
        A[2 * (i), 0] = x1[i]
        A[2 * (i), 1] = y1[i]
        A[2 * (i), 2] = 1
        A[2 * (i), 3] = -x2[i] * x1[i]
        A[2 * (i), 4] = -x2[i] * y1[i]

        A[2 * (i) + 1, 3] = -y2[i] * x1[i]
        A[2 * (i) + 1, 4] = -y2[i] * y1[i]
        A[2 * (i) + 1, 5] = x1[i]
        A[2 * (i) + 1, 6] = y1[i]
        A[2 * (i) + 1, 7] = 1

        B[2 * (i)] = x2[i]
        B[2 * (i) + 1] = y2[i]

    AtA = np.dot(A.T, A)
    AtB = np.dot(A.T, B)
    AtAi = np.linalg.inv(AtA)
    Xe = np.dot(AtAi, AtB)
    print('raw Coef of ', Rgt, '-', Lft, 'model :')
    print(Xe)

    one = np.array([1])
    Xe = np.vstack((Xe, one))

    print('normalized Coef of ', Rgt, '-', Lft, 'model :')
    print(Xe)

    print('The End')
    exit()
