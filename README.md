**Intro**

This readme provides an explanation of the data and code developed for the master’s thesis with the topic: “Erkennung und Tracking von Fußgängern
unter Verwendung eines 360◦ Kamerafusionssystems”

**General Structure**

The files are structured as follows:

- data: this folder includes the annotated labeled data and JSON files in YOLO format.
- detection: script used for detecting pedestrians in images
- preparing-data: used to read the .bag files to extract images / converting the uni-data and woodscape dataset images and labels to YOLOv4 format.
- Projection:
  - map_fisheye.py used for testing the calculated coordinates
  - proj_model_fisheye.py  is used to calculate the projection points 
  - Utils.py projection between neighboring cameras
- Tracking: deepsort files
  - main.py : is the core and main program, performs detection,projection,id-fusion and tracking
- Training_YOLOv4: yolo parameters

Model has been trained on Google Colab using the pedestrian_detector.ipynb 


