# https://github.com/erentknn/yolov4-object-detection/blob/master/yolo_image.py

import cv2
import numpy as np

CONFIDENCE_THRESHOLD = 0.3
NMS_THRESHOLD = 0.1


weights = "trainig-YOLOv4/yolov4-custom_best.weights"
labels = ["pedestrian"]
cfg = "tracking/utilies/yolov4-custom_test.cfg"

print("You are now using {} weights ,{} configs and {} labels.".format(weights, cfg, labels))

lbls = labels

COLORS = np.random.randint(0, 255, size=(len(lbls), 3), dtype="uint8")

net = cv2.dnn.readNetFromDarknet(cfg, weights)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

layer = net.getLayerNames()
layer = [layer[i - 1] for i in net.getUnconnectedOutLayers()]


def detect(image, nn):

    (H, W) = image.shape[:2]

    blob = cv2.dnn.blobFromImage(image, 1 / 255, (608, 608), swapRB=True, crop=False)
    nn.setInput(blob)
    layer_outs = nn.forward(layer)

    boxes = list()
    confidences = list()
    class_ids = list()

    for output in layer_outs:
        for detection in output:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]

            if confidence > CONFIDENCE_THRESHOLD:
                box = detection[0:4] * np.array([W, H, W, H])
                (center_x, center_y, width, height) = box.astype("int")

                x = int(center_x - (width / 2))
                y = int(center_y - (height / 2))

                boxes.append([x, y, int(width), int(height)])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    idxs = cv2.dnn.NMSBoxes(boxes, confidences, CONFIDENCE_THRESHOLD, NMS_THRESHOLD)
    return_boxes = []
    return_scores = []
    return_class_names = []
    if len(idxs) > 0:

        for i in idxs.flatten():
            return_boxes.append([boxes[i][0], boxes[i][1], boxes[i][2], boxes[i][3]])
            return_scores.append(confidences[i])
            return_class_names.append(class_id)

    return return_boxes, return_scores, return_class_names

