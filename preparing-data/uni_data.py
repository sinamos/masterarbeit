# original code @ https://github.com/qqwweee/keras-yolo3/blob/master/coco_annotation.py

import os
import json
import random



def cls_chck(cat):

    if cat >= 1 and cat <= 11:
        cat = cat - 1
    elif cat >= 13 and cat <= 25:
        cat = cat - 2
    elif cat >= 27 and cat <= 28:
        cat = cat - 3
    elif cat >= 31 and cat <= 44:
        cat = cat - 5
    elif cat >= 46 and cat <= 65:
        cat = cat - 6
    elif cat == 67:
        cat = cat - 7
    elif cat == 70:
        cat = cat - 9
    elif cat >= 72 and cat <= 82:
        cat = cat - 10
    elif cat >= 84 and cat <= 90:
        cat = cat - 11
    return cat

def extract_from_json(path_image, output_txt, path_json_file):
    """

    :param path_image: path of image file
    :param output_image: destination address for save text file (label)
    :param path_json_file: path of JSON file
    :return: creates a text file that each line has information about the label
    """
    id_img = {}
    id_ann = {}
    print("sina")
    f_out = open(output_txt, 'w+')
    count = 1
    with open(path_json_file) as f_ann:
        jf = json.load(f_ann)
    for img in jf['images']:
        id_img[img['id']] = img['file_name']
    for ann in jf['annotations']:

        bbox = ann['bbox']
        # convert x,y,w,h to x1,y1,x2,y2
        bbox = [bbox[0], bbox[1], bbox[0] + bbox[2], bbox[1] + bbox[3]]
        bbox = map(int, bbox)
        bbox = ','.join(map(str, bbox)).strip('[]')
        bbox = bbox.replace(' ', '')
        count += 1
        if ann['image_id'] in id_ann.keys():
            id_ann[ann['image_id']] += [bbox, str(cls_chck(ann['category_id']))]
        else:
            id_ann[ann['image_id']] = [bbox, str(cls_chck(ann['category_id']))]

    for img in id_img.keys():

        if img not in id_ann.keys():
            print('image ' + id_img[img] + ' does not have any annotation!')
            continue
        ann_line = ''
        ann_line += path_image + id_img[img].replace("\\", "/")

        for i in range(0, len(id_ann[img]), 2):
            ann_line += ' ' + id_ann[img][i] + ',' + id_ann[img][i + 1]  # + ',' + id_ann[img][i + 2]

        print(ann_line, file=f_out)

    f_out.close()
    print(count, "anzahl")

import json


def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)


def change_format(path_txt, output_path, image_size):
    """

    :param path_txt: path of text lable file
    :param output_path: destination address for save text file (label)
    :param image_size:
    :return:
    """

    w= int(image_size[0])
    h= int(image_size[1])

    with open(path_txt) as f:
        lines = f.readlines()

        for i in lines:
            data = i.split(" ")

            print(data)
            name = data[0].split("/")[3][:-5] + ".txt"
            new_file = open(output_path + "{}".format(name), "w+")
            for i in data[1:]:
                bb = (i.split(",")[:4])
                b = (int(bb[0]), int(bb[2]), int(bb[1]), int(bb[3]))
                bb = convert((w, h), b)
                new_file.write("0 {} {} {} {} \n".format(bb[0], bb[1], bb[2], bb[3]))
            new_file.close()


def create_train_valid(path_annotation, output_save, path_image,file_format, valid_percentage):

    """

    :param path_annotation: path of labeled txt file
    :param output_save: destination address for save file
    :param path_image: images address
    :param file_format: the format of data
    :param valid_percentage: Percentage of validation of dataset
    :return: creates 2 txt files valid and train
    """

    files = os.listdir(path_annotation)#os.listdir("d:/front_ann/")

    data = [ (random.random(), line) for line in files ]

    data.sort()


    def percentage(part, whole):
      return float(whole) * (float(part)/100)

    valid_num = round(percentage(valid_percentage, len(data)))
    count = 0
    with open(output_save +'train_front.txt','w') as train, open(output_save +'valid_front.txt','w') as valid:
        for _, line in data:
            filename = line.split(".")[0]
            if count <= valid_num:
                valid.write(path_image + filename + ".{}".format(file_format) + "\n")
            elif count > valid_num:
                train.write(path_image + filename + ".{}".format(file_format)  + "\n")
            count += 1

extract_from_json("D:/label/front/", "D:/label/front_uni.txt", "D:/label/front.json")
change_format("D:/label/front_uni.txt", "D:/label/front_ann/", [1280, 800])
create_train_valid("D:/label/front_ann/","D:/label/", "D:/","jpeg", 100)
