
import os
import rosbag
import numpy as np
import cv2

path_save = "E:/"
bag_name = "E:/20170324-122551_teufelsberg_downhill_filtered.bag"
topic_front = "/sensors/broadrreachcam_front/image_compressed/compressed"
topic_right = "/sensors/broadrreachcam_right/image_compressed/compressed"
topic_rear = "/sensors/broadrreachcam_rear/image_compressed/compressed"
topic_left = "/sensors/broadrreachcam_left/image_compressed/compressed"
name_bag_file = os.path.basename(bag_name)[:-4]

try:
    os.mkdir(path_save + name_bag_file)
    print(" directory", name_bag_file, "created")
    path_save += name_bag_file + "/"

except FileExistsError:
    print("Directory", name_bag_file, "already exists")

def main():


    # read Bag file
    bag = rosbag.Bag(bag_name)
    last_topic = None
    counter = 0
    flag_left, flag_right, flag_rear, flag_front = False, False, False, False

    # Reading all the topics present in the bag file
    for topic, msg, t in bag.read_messages():
        name = topic[23:29]


        # Based on photos with same time-stamp from 4 different cameras
        if hasattr(msg,"header"):
            time = msg.header.stamp

        if last_topic == topic:
            continue

        if topic == topic_front:

            np_array = np.frombuffer(msg.data, np.uint8)
            Image_front = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
            last_topic = topic
            counter += 1
            flag_front = True

        elif topic == topic_right:

            np_array = np.frombuffer(msg.data,np.uint8)
            Image_right = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
            last_topic = topic
            counter += 1
            flag_right = True

        elif topic == topic_rear:

            np_array = np.frombuffer(msg.data,np.uint8)
            Image_rear = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
            last_topic = topic
            counter += 1
            flag_rear = True

        elif topic == topic_left:


            np_array = np.frombuffer(msg.data,np.uint8)
            Image_left = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
            last_topic = topic
            counter += 1
            flag_left = True
        # Saving images in groups of 4 from 4 cameras only if images have the same time stamp
        if counter == 4:
            if flag_left and flag_rear and flag_front and flag_right == True:
                cv2.imwrite(os.path.join(path_save, str(time) + "_front" + "_camera" + ".jpeg"), Image_front)
                cv2.imwrite(os.path.join(path_save, str(time) + "_right" + "_camera" + ".jpeg"), Image_right)
                cv2.imwrite(os.path.join(path_save, str(time) + "_rear" + "_camera" + ".jpeg"), Image_rear)
                cv2.imwrite(os.path.join(path_save, str(time) + "_left" + "_camera" + ".jpeg"), Image_left)
            counter = 0
            flag_left, flag_right, flag_rear, flag_front = False, False, False, False
    rospy.init_node("Image_extractor")
    bag.close()
    rospy.spin()

if __name__ == "__main__":
    main()