from random import random
from PIL import Image
import os

def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)



def save_pedestrian(path_annotation, output_path, image_size):
    """

    :param path_annotation: path of labeled txt file
    :param output_path: destination address for save file
    :param image_size: the size of width and height of image [w, h]
    :return: Creating a text file for each photo with class, bounding box coordinates and width and height (normalized)
    """

    files = os.listdir(path_annotation)
    w= int(image_size[0])
    h= int(image_size[1])
    for file in files:
        with open(path_annotation + "{}".format(file)) as f:
            new_file= open(output_path+"{}".format(file),"w+")
            lines = f.readlines()
            for i in lines:
                s = i.splitlines()[-1]
                s = s.split(",")

                if s[0] == "person":

                    b = (int(s[2]), int(s[4]), int(s[3]), int(s[5]))
                    bb = convert((w,h), b)

                    new_file.write("0 {} {} {} {} \n".format(bb[0], bb[1], bb[2], bb[3]))

            new_file.close()


def remove_empty_file(path_annotation):
    """
    :param path_annotation: path of labeled txt file
    :return: delete the text data in which there is no class of pedestrian
    """
    files = os.listdir(path_annotation)
    for file in files:
        filesize = os.path.getsize("{}".format(path_annotation+ file))
        if filesize == 0:
            os.remove("{}".format(path_annotation+ file))
        else:
            print("The file is not empty: " + str(filesize))
remove_empty_file(path_annotation="E:/rgb_images/test/")

def create_train_valid(path_annotation, output_save, path_image,file_format, valid_percentage):

    """

    :param path_annotation: path of labeled txt file
    :param output_save: destination address for save file
    :param path_image: images address
    :param file_format: the format of data
    :param valid_percentage: Percentage of validation of dataset
    :return: creates 2 txt files valid and train
    """

    files = os.listdir(path_annotation)#os.listdir("d:/front_ann/")

    data = [ (random.random(), line) for line in files ]

    data.sort()


    def percentage(part, whole):
      return float(whole) * (float(part)/100)

    valid_num = round(percentage(valid_percentage, len(data)))
    count = 0
    with open(output_save +'train_front.txt','w') as train, open(output_save +'valid_front.txt','w') as valid:
        for _, line in data:
            filename = line.split(".")[0]
            if count <= valid_num:
                valid.write(path_image + filename + ".{}".format(file_format) + "\n")
            elif count > valid_num:
                train.write(path_image + filename + ".{}".format(file_format)  + "\n")
            count += 1

